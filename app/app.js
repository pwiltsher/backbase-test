import angular from 'angular'
import WeatherCtrl from './weather.controller.js'

import '../scss/app.scss'

let app = () => {
  return {
    template: require('./weather.html')
  }
}

angular
  .module('app', [])
  .directive('app', app)
  .controller('WeatherCtrl', ['$scope', '$interval', '$http', WeatherCtrl])

export default app
