import cities from '../data/cities'

function WeatherCtrl ($scope, $interval, $http) {
  const vm = this
  vm.list = []
  vm.loaded = false
  vm.forecast = -1
  const dateOpts = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  vm.date = ''

  const init = function () {
    cities.forEach((city, index) => {
      loadCityData(city)
    })
    const date = new Date()
    setDate(date)
    $interval(() => {
      date.setSeconds(date.getSeconds() + 1)
      setDate(date)
    }, 1000)
  }

  const setDate = date => (vm.date = date.toLocaleDateString('en-GB', dateOpts))

  const loadCityData = function (city) {
    const url =
      'http://api.openweathermap.org/data/2.5/weather?APPID=3d8b309701a13f65b660fa2c64cdc517&q=' +
      city
    $http.get(url).then(function (response) {
      const data = response.data
      data.city = city
      vm.list.push(data)
      if (vm.list.length === cities.length) {
        vm.loaded = true
      }
    })
  }

  const loadCityForecast = function (city, index, cb) {
    const url =
      'http://api.openweathermap.org/data/2.5/forecast?APPID=3d8b309701a13f65b660fa2c64cdc517&q=' +
      city
    $http.get(url).then(function (response) {
      const data = response.data
      cb(index, data)
    })
  }

  const addForecastData = function (cityIndex, data) {
    const d = Math.round(new Date().getTime() / 1000)
    const tomorrow = d + 24 * 3600
    const forecast = data.list.filter(f => f.dt <= tomorrow)

    vm.list[cityIndex].forecast = forecast
    vm.list[cityIndex].showforecast = true
  }

  vm.tempToCelsius = function (kelvin) {
    const temp = kelvin - 273.15
    return Math.round(temp)
  }

  vm.toggleForecast = function (index) {
    const city = vm.list[index]
    if (typeof city.forecast === 'undefined') {
      loadCityForecast(city.city, index, addForecastData)
    } else {
      vm.list[index].showforecast = !vm.list[index].showforecast
    }
  }

  init()
}

export default WeatherCtrl
