# Setup & Running

```
yarn
yarn run build
yarn run start
```

This should fire up a local server accessible at http://localhost:9000

# Approach

Coming from React and not using Angular for at least 2 years, a lot of this little app was me trying to discover best practices for structure and layout of an angular app, while keeping it as clean as possible.  As this is a relatively small app, I have combined the angular logic within one folder (`app`) and only used one controller to handle all functionality.

Originally I had planned to extract out the API integration into a separate file, utilising `async/await` to handle the calls to the Open Weather API but I couldn't make it work within angular's scoping, so ended up integrating with the `$http` service to handle the calls, and utilising callback functions when done to manipulate the data.

The files are run through webpack to transform the ES6 elements, with the output generated into a `dist` folder.  The compiled code is then referenced in the main `index.html` file.  I chose SASS as the CSS preprocessor as it's one I have used fairly regularly, although with React being component focused I mostly use css-in-js nowadays.

I'm almost certain there are multiple better ways of architecting Angular apps, but I hope I have shown enough here to express my knowledge of JavaScript as a language and that you see enough promise to consider cross training me into Angular.
